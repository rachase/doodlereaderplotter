#include "DisGui.h"

bool show_test_window;

DisGui::DisGui()
{
	SetupGui();
}

//https://github.com/ocornut/imgui/blob/master/docs/FONTS.md

void DisGui::setup_font()
{
	ImGuiIO& io = ImGui::GetIO();
	std::string fp = "/usr/share/fonts/truetype/cmu/cmunss.ttf";
	ofFile f(fp);
	
	if(f.doesFileExist(f.getAbsolutePath()))
	{
		//io.Fonts->AddFontFromFileTTF(fp.c_str(), 14);	
		io.Fonts->AddFontFromFileTTF(fp.c_str(), 24);	
		//io.Fonts->AddFontFromFileTTF(fp.c_str(), 32);	
	}
	
}

void DisGui::SetupGui()
{
	setup_font();

	//set window properties
	static bool no_titlebar = false;
	//static bool no_border = true;
	static bool no_resize = true;
	static bool no_move = true;
	static bool no_scrollbar = false;
	static bool no_collapse = true;
	static bool no_menu = true;
	static bool no_settings = true;
	//static float bg_alpha = -0.01f; // <0: default
	//bool show = true;

	// Demonstrate the various window flags.
	// Typically you would just use the default.
	ImGuiWindowFlags window_flags = 0;
	if (no_titlebar)
		window_flags |= ImGuiWindowFlags_NoTitleBar;
	//if (!no_border)   window_flags |= ImGuiWindowFlags_ShowBorders;
	if (no_resize)
		window_flags |= ImGuiWindowFlags_NoResize;
	if (no_move)
		window_flags |= ImGuiWindowFlags_NoMove;
	if (no_scrollbar)
		window_flags |= ImGuiWindowFlags_NoScrollbar;
	if (no_collapse)
		window_flags |= ImGuiWindowFlags_NoCollapse;
	if (!no_menu)
		window_flags |= ImGuiWindowFlags_MenuBar;
	if (no_settings)
		window_flags |= ImGuiWindowFlags_NoSavedSettings;

}

void DisGui::GuiDisplay()
{
	// Display GUI
	begin();

	ImGui::Checkbox("Show Test Window", &show_test_window);
	if (show_test_window)
	{
		ImGui::ShowDemoWindow(&show_test_window);
	}

	ImGui::Spacing();
	ImGui::Checkbox("Single Doodle", &single_doodle);
	ImGui::SliderInt("DoodleOffset",&read_doodle_offset, 0, 1000);
	ImGui::SliderFloat("viewscale", &viewscale, .1, 2);
	ImGui::Spacing();
	ImGui::SliderInt("Rows", &num_rows, 1, 50);
	ImGui::SliderInt("Columns", &num_cols, 1, 50);
	ImGui::Spacing();
	ImGui::Checkbox("global rotate", &global_rotation);
	ImGui::SliderFloat("rot x", &rotation_x, 0, 360);
	ImGui::SliderFloat("rot y", &rotation_y, 0, 360);
	ImGui::SliderFloat("rot z", &rotation_z, 0, 360);
	ImGui::Spacing();
	ImGui::SliderFloat("Margin:Sides", &margin_sides, 0, 1);
	ImGui::SliderFloat("Margin:Top/Bot", &margin_topbot, 0, 1);
    ImGui::Spacing();
	
	ImGui::ShowFontSelector("font");
    
	
	//ImGui::InputText("PATH", gui_filepath, sizeof(gui_filepath), 0);


	static std::string viewname = "DOODLE DROP DOWN";
	if (doodle_filenames.size() <= 0)
	{
		viewname = "DOODLE DROP DOWN";
	}
	else
	{
		viewname = doodle_filenames.at(doodle_file_current_idx).getFileName().c_str();
	}
	if (ImGui::BeginCombo("combo 1", viewname.c_str(), 0))
	{
		for (unsigned int n = 0; n < doodle_filenames.size(); n++)
		{
			const bool is_selected = (doodle_file_current_idx == n);
			if (ImGui::Selectable(doodle_filenames.at(n).getFileName().c_str(), is_selected))
			{
				doodle_file_current_idx = n;
			}

			// Set the initial focus when opening the combo (scrolling + keyboard navigation focus)
			if (is_selected)
			{
				ImGui::SetItemDefaultFocus();
			}
		}
		ImGui::EndCombo();
	}
	end();
}
