#pragma once
#include <stdint.h>
#include "ofMain.h"
#include "ofxGui.h"
#include "ofxImGui.h"

class DisGui : public ofxImGui::Gui
{

public:
  // Generic Gui Components
  void GuiDisplay(void); //For organization purposes
  void SetupGui(void);   //initial setup parameters

  bool DIAGNOSTICS = TRUE;
  
  DisGui();

  //variables for this GUI

  float viewscale=1.0;
	int num_rows=11;
  int num_cols=17;
  float margin_sides=.1;
  float margin_topbot=.1;
  int read_doodle_offset=0;
  
  float rotation_x;
  float rotation_y;
  float rotation_z;

  bool global_rotation = true; 
  bool single_doodle = true; 
  void setup_font(void);

  std::string doodles_filepath;
  std::vector<ofFile> doodle_filenames;
  //Here we store our selection data as an index.
  unsigned int doodle_file_current_idx = 0; 

};
