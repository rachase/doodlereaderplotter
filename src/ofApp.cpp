#include "ofApp.h"

ofRectangle ofApp::getBoundingBoxOfPath(ofPath &path)
{
    ofRectangle rect;
    for (int i = 0; i < path.getOutline().size(); i++)
    {
        ofRectangle b = path.getOutline().at(i).getBoundingBox();
        if (i == 0)
            rect = b;
        else
            rect.growToInclude(b);
    }
    return rect;
}

void ofApp::setup_grid()
{
    glm::vec3 p; // create a point P
    p.x = 225;   // set the x of the point
    p.y = 10;    // set the y of the point

    int paper_width = 1100;
    int paper_height = 1700;

    // push the current style
    ofPushStyle();

    ofNoFill();
    
    // Draw the paper
    ofDrawRectangle(p, paper_width, paper_height);

    // Draw the margins
    float x_adj = ofMap(gui.margin_sides, 0, 1, 0, 400, true);
    ofDrawLine(p.x + x_adj, p.y, p.x + x_adj, p.y + paper_height);
    ofDrawLine(p.x + paper_width - x_adj, p.y, p.x + paper_width - x_adj, p.y + paper_height);

    float y_adj = ofMap(gui.margin_topbot, 0, 1, 0, 400, true);
    ofDrawLine(p.x, p.y + y_adj, p.x + paper_width, p.y + y_adj);
    ofDrawLine(p.x, p.y + paper_height - y_adj, p.x + paper_width, p.y + paper_height - y_adj);

    //figure out the inner grid
    //the inner rect
    ofRectangle inner_rect;
    inner_rect.x = p.x + x_adj;
    inner_rect.y = p.y + y_adj;
    inner_rect.width = paper_width - 2 * x_adj;
    inner_rect.height = paper_height - 2 * y_adj;

    //draw the inner rect and boxes
    ofDrawRectangle(inner_rect);

    ofRectangle grid_element;
    grid_element.x = p.x + x_adj;
    grid_element.y = p.y + y_adj;
    grid_element.width = inner_rect.width / gui.num_cols;
    grid_element.height = inner_rect.height / gui.num_rows;
    
    //for every column
    for (int i = 0; i < gui.num_cols; i++)
    {
        //for every row
        for (int j = 0; j < gui.num_rows; j++)
        {
            ofDrawRectangle(grid_element);
            grid_element.y = p.y + y_adj + j * grid_element.height;
        }
        grid_element.x = p.x + x_adj + i * grid_element.width;
    }

    // recall the pushed style
    ofPopStyle();
}

//--------------------------------------------------------------
void ofApp::setup()
{
    ofEnableSmoothing();
    ofSetLineWidth(2);

    //setup the GUI
    gui.setup();
    check_doodle_dir_get_filenames();

    save_to_svg_flag=false;

    loaddoodlestomem("/media/richard/New Volume/CAD/googledoodles/binary/bee.bin");
}

void ofApp::check_doodle_dir_get_filenames()
{

    std::vector<ofFile> thesefilenames;

    //ofFilePath filepath();
    ofDirectory dir;
    dir.open("/media/richard/New Volume/CAD/googledoodles/binary/");

    thesefilenames.clear();

    if (dir.isDirectory())
    {
        int numFiles = dir.listDir();
        for (int i = 0; i < numFiles; ++i)
        {
            std::string s = dir.getPath(i);
            ofFile f(s);
            thesefilenames.push_back(f);
        }
    }

    //copy the filenames over to the gui
    gui.doodle_filenames = thesefilenames;


}

//---------------------------------------------------------
void ofApp::update()
{
    static int last_file_index = 0;
    if (last_file_index != gui.doodle_file_current_idx)
    {
        loaddoodlestomem(gui.doodle_filenames.at(gui.doodle_file_current_idx).path());
        last_file_index = gui.doodle_file_current_idx;
    }

    static int last_num_ros=0,last_num_cols=0;
    if ((last_num_cols!=gui.num_cols)|(last_num_ros!=gui.num_rows))
    {
        calc_rotation_values();
    }
    
}

void ofApp::set_path_style(ofPath *p)
{
    p->setFilled(false);
    p->setStrokeColor(ofColor::black);
    //p->setStrokeWidth(gui.linewidth);
    p->setStrokeWidth(2.0);
    //p->setMode(ofPath::POLYLINES);
    p->setMode(ofPath::COMMANDS);
}

void ofApp::loaddoodlestomem(std::string filepath)
{
    ofFile f;
    ofPath thispath;


    std::vector<float> xs;
    std::vector<float> ys;

    unsigned long long key_id = 0; //8 bytes
    char countrycode[2];           //2 bytes
    signed char recognized;        //1 bytes
    unsigned int timestamp;        //4 bytes
    unsigned short n_strokes;      //2 bytes

    unsigned short n_points; //2 bytes

    std::cout << "Opening File [%s]" << filepath << std::endl;

    f.open(filepath, ofFile::ReadOnly, true); //open the file

    if (!f.good())
    {
        printf("File Not Good\n\r");
        ofExit();
    }

    recognized = false;
    doodle_paths.clear();    

    //read everything into a vector
    while (!f.eof())
    {
        f.read((char *)&key_id, 8);
        f.read((char *)&countrycode, 2);
        f.read((char *)&recognized, 1);
        f.read((char *)&timestamp, 4);
        f.read((char *)&n_strokes, 2);

        //printf("Got KeyID %lu, CC %C%C, Rec:%d, Time %u, Strokes %d\n\r",
        //    key_id,countrycode[0],countrycode[1],recognized,timestamp,n_strokes);

        thispath.clear();
        xs.clear();
        ys.clear();

        for (unsigned int i = 0; i < n_strokes; i++)
        {
            unsigned char x;
            unsigned char y;
            f.read((char *)&n_points, 2);
            //printf("Found %d points in stroke %d\n\rX's = ",n_points,i);
            for (int j = 0; j < n_points; j++)
            {
                f.read((char *)&x, 1);
                xs.push_back(float(x));
                //printf("%d,",x);
            }
            //printf("\n\rY's = ");

            for (unsigned int j = 0; j < n_points; j++)
            {
                f.read((char *)&y, 1);
                ys.push_back(float(y));
                //printf("%d,",y);
            }

            thispath.moveTo(xs.at(0), ys.at(0), 0);

            for (unsigned int j = 0; j < n_points; j++)
            {
                thispath.lineTo(xs.at(j), ys.at(j), 0);
            }

            xs.clear();
            ys.clear();
            //printf("\n\r");
        }
        set_path_style(&thispath);
        //only want the ones recognized by the nueral net;
        if(recognized)doodle_paths.push_back(thispath);
    }

    std::cout << "Done Loading File" << std::endl;
}

//reads the doodles, has modulo operator for idx

ofPath ofApp::readDoodle(unsigned int idx)
{
    unsigned int n;
    n = (idx+read_doodle_offset)%doodle_paths.size();
    ofPath p;
    if(n>=doodle_paths.size())
    {
        n = 0;
    }
    p = doodle_paths.at(n);
    return p;
}

void ofApp::calc_rotation_values()
{
    rotation_degrees.clear();
    unsigned int num = gui.num_cols*gui.num_rows;

    for (int i = 0; i < num; i++)
    {
        rotation_degrees.push_back(i);
    }
}

void ofApp::draw()
{
    ofSetBackgroundColor(ofColor::white);
    gui.draw();

    //ofSetColor(ofColor::black);
    if (save_to_svg_flag == 1) 
    {
        ofBeginSaveScreenAsSVG("test.svg", false, true);
    }

    //gui_filenames.draw();
    draw_paper();

    //ofSetHexColor(0x0000ff);
    ofNoFill();

    for (int i = 0; i < subgrid.size(); i++)
    {
        //draw the subgrid
        //ofDrawRectangle(subgrid.at(i));

        //get the new doodle
        ofPath doodle;
        if(gui.single_doodle == true)
        {
            doodle = readDoodle(1);
        }
        else
        {
            doodle = readDoodle(i);
        }
        

        //get the bounding box of the doodle
        ofRectangle bb_box = getBoundingBoxOfPath(doodle);

        //scale the doodle to fit in the subgrid element
        float x_scale = bb_box.width / subgrid.at(i).width;
        float y_scale = bb_box.height / subgrid.at(i).height;

        //find the max proportion
        float scale = glm::max(x_scale, y_scale);
   
        //fit it to % window size
        scale = scale / .9;
        scale = gui.viewscale/scale;
        doodle.scale(scale,scale);
        
        float scaled_float =
            (subgrid.at(i).getCenter().x/(subgrid.at(i).getWidth()*gui.num_cols))*.5 +
            (subgrid.at(i).getCenter().y/(subgrid.at(i).getHeight()*gui.num_rows))*.5;

        if(gui.global_rotation == true)
        {
            //scale everything the same.
            doodle.scale(((360-2*gui.rotation_x)/360.0),((360-2*gui.rotation_y)/360.0));
            doodle.rotate(gui.rotation_z,glm::vec3(0,0,1)); 
        }
        else
        {
            //scale it based on position
            //scaled_float = (scaled_float*2)-.5;
            //doodle.scale(scaled_float*((360-2*gui.rotation_x)/360.0),scaled_float*((360-2*gui.rotation_y)/360.0));
            //doodle.scale(scaled_float*((360-2*gui.rotation_x)/360.0),1);
            doodle.rotate(scaled_float*gui.rotation_x,glm::vec3(1,0,0));
            doodle.rotate(scaled_float*gui.rotation_y,glm::vec3(0,1,0));
            doodle.rotate(scaled_float*gui.rotation_z,glm::vec3(0,0,1)); 
        } 
        
        //now that the doodle is scale, get the new bounding box
        bb_box = getBoundingBoxOfPath(doodle);

        //make a point and setup translation
        ofPoint p = subgrid.at(i).getCenter() - bb_box.getCenter();

                 
        doodle.translate(p);

        doodle.draw();

    }

    if (save_to_svg_flag)
    {
        ofEndSaveScreenAsSVG();
        save_to_svg_flag = false;
    }

    if (gui.DIAGNOSTICS)DisplayDiagnostics();
    
    gui.GuiDisplay();

    read_doodle_offset = gui.read_doodle_offset;

}

//--------------------------------------------------------------
void ofApp::keyPressed(int key)
{
    switch (key)
    {
    case ' ':
        save_to_svg_flag = true;
        break;
    }
}

void ofApp::draw_paper()
{
    glm::vec3 p; // create a point P
    p.x = 10;   // set the x of the point
    p.y = 10;    // set the y of the point

    int paper_width = 1700;
    int paper_height = 1100;

    subgrid.clear();

    // push the current style
    ofPushStyle();

    ofNoFill();
    ofSetLineWidth(1);       // set line width to 1

    // Draw the paper
    ofDrawRectangle(p, paper_width, paper_height);

    // Draw the margins
    float x_adj = ofMap(gui.margin_sides, 0, 1, 0, 400, true);
    ofDrawLine(p.x + x_adj, p.y, p.x + x_adj, p.y + paper_height);
    ofDrawLine(p.x + paper_width - x_adj, p.y, p.x + paper_width - x_adj, p.y + paper_height);

    float y_adj = ofMap(gui.margin_topbot, 0, 1, 0, 400, true);
    ofDrawLine(p.x, p.y + y_adj, p.x + paper_width, p.y + y_adj);
    ofDrawLine(p.x, p.y + paper_height - y_adj, p.x + paper_width, p.y + paper_height - y_adj);

    //figure out the inner grid
    //the inner rect
    ofRectangle inner_rect;
    inner_rect.x = p.x + x_adj;
    inner_rect.y = p.y + y_adj;
    inner_rect.width = paper_width - 2 * x_adj;
    inner_rect.height = paper_height - 2 * y_adj;

    //draw the inner rect and boxes
    //ofSetHexColor(0x00ffff);
    //ofDrawRectangle(inner_rect);

    ofRectangle grid_element;
    grid_element.x = inner_rect.x;
    grid_element.y = inner_rect.y;
    grid_element.width = inner_rect.width / gui.num_cols;
    grid_element.height = inner_rect.height / gui.num_rows;
    
    //for every column
    for (int i = 0; i < gui.num_cols; i++)
    {
        grid_element.x = inner_rect.x + i * grid_element.width;
        //for every row
        for (int j = 0; j < gui.num_rows; j++)
        {
            //ofDrawRectangle(grid_element);
            grid_element.y = inner_rect.y + j * grid_element.height;
            subgrid.push_back(grid_element);
        }
    }

    // recall the pushed style
    ofPopStyle();
}

void ofApp::DisplayDiagnostics()
{
    string fpsStr;
    int base_x = 100, base_y = 100;
    int space = 12;
    int i = 0;

    base_x = ofGetWindowWidth() - 260;
    base_y = ofGetWindowHeight() - 75;
    ofSetColor(0, 0, 0, 255);
    i++;

    //frame rate Statistics
    fpsStr = "frame rate/target: " + ofToString(ofGetFrameRate(), 2) + '/' + ofToString(ofGetTargetFrameRate(), 2);

    ofDrawBitmapString(fpsStr, base_x, base_y + space * i);
    i++;

    fpsStr = "frame num: " + ofToString(ofGetFrameNum(), 2);
    ofDrawBitmapString(fpsStr, base_x, base_y + space * i);
    i++;

    fpsStr = "last frame time: " + ofToString(ofGetLastFrameTime(), 2);
    ofDrawBitmapString(fpsStr, base_x, base_y + space * i);
    i++;

    //Window Statistics
    fpsStr = "window h/w: " + ofToString(ofGetWindowHeight(), 2) + '/' + ofToString(ofGetWindowWidth(), 2);
    ofDrawBitmapString(fpsStr, base_x, base_y + space * i);
    i++;

    fpsStr = "window pos x/y: " + ofToString(ofGetWindowPositionX(), 2) + '/' + ofToString(ofGetWindowPositionY(), 2);
    ofDrawBitmapString(fpsStr, base_x, base_y + space * i);
    i++;
}
