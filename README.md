# doodlereaderplotter

Reads in Google Quick Draw Dataset and converts them to plottable svgs. Has a few feature such as paper size, rotations, scaling, and matrix size.  
Uses cmu font if available `sudo apt install fonts-cmu`.  Looks for files in `/usr/share/fonts/truetype/cmu`  

<img src="screen.png"  width="800" >
